import React from 'react';

const ComboBox = props => {
  const { data, onSelectItemChange, valueField } = props;

  return (
    <div>
      <select onChange={onSelectItemChange} value={valueField}>
        <option>Choose Your car</option>
        {data.map(item => (
          <option key={item.id}>{item.name}</option>
        ))}
      </select>
    </div>
  );
};

export default ComboBox;
