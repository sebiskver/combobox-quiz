import React, { Component } from 'react';
import './App.css';
import ComboBox from './components/ComboBox/Combobox';

class App extends Component {
  state = {
    cars: [
      { id: 1, name: 'BWM M3' },
      { id: 2, name: 'MacLaren P1' },
      { id: 3, name: 'Audi TT RS' }
    ],

    valueField: ''
  };

  handleChangeItem = event => {
    const value = event.target.value;
    this.setState({ valueField: value });
  };

  render() {
    const { cars, valueField } = this.state;

    return (
      <div className="combobox-wrapper">
        <ComboBox
          data={cars}
          valueField={valueField}
          onSelectItemChange={this.handleChangeItem}
        />
      </div>
    );
  }
}

export default App;
